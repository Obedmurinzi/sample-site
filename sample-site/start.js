var express = require('express');
var router = express.Router();
var mongodb = require('mongodb');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/thelist', function(req, res){
 
  // Get a Mongo client to work with the Mongo server
  var MongoClient = mongodb.MongoClient;
 
  // Define where the MongoDB server is
  var url = 'mongodb://localhost:27017/bakery';
 
  // Connect to the server
  MongoClient.connect(url, function (err, client) {
  if (err) {
    console.log('Unable to connect to the Server', err);
  } else {
    // We are connected
    console.log('Connection established to', url);
 
    // Get the documents collection
    
    var db = client.db('samplesite');
 
    
    db.collection('ingredients').find({}).toArray(function (err, result) {
      if (err) {
        res.send(err);
      } else if (result.length) {
       res.send(result);
       
      } else {
        res.send('No documents found');
      }
      //Close connection
      client.close();
    });
  }
  });
});
 

router.get('/newingredients', function(req, res){
    res.render('newingredients', {title: 'Add ingredients' });
});
 
router.post('/addingredients', function(req, res){
 
    // Get a Mongo client to work with the Mongo server
    var MongoClient = mongodb.MongoClient;
 
    // Define where the MongoDB server is
    var url = 'mongodb://localhost:27017/sampsite';
 
    // Connect to the server
    MongoClient.connect(url, function(err, db){
      if (err) {
        console.log('Unable to connect to the Server:', err);
      } else {
        console.log('Connected to Server');
 
        // Get the documents collection
        var collection = db.collection('ingredients');
 
      
        var Milk = {name: req.body.Milk, quantity: req.body.quantity,
          metrics: req.body.metrics, dateofpurchase: req.body.dateofpurchase, totalcost: req.body.totalcost,
          costperunit: req.body.costperunit};
 
        
        collection.insert([student1], function (err, result){
          if (err) {
            console.log(err);
          } else {
 
          
            res.redirect("thelist");
          }
 
          // Close the database
          db.close();
        });
 
      }
    });
 
  });

module.exports = router;